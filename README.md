TODO:
* make an upload script
* create a system for entering questions easily
* when clicking back make free text bring up the text that was previously written
* refactor answer answer object in question html
* make freetext questions clean text after question
* refer to question names only once in consts
* parse answers to/from graph better
* reference answers by id
* refactor medical summary
* output graph object like gojs expects

***instructions for running locally:***

1. run npm install
2. run webpack (maybe need to install locally via npm)
3. open index.html with browser

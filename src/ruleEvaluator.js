let summary = require('./summary/summary')
let tree = require('./tree/tree')

function getNextQuestionId(question) {
    let rule = getFirstAppliedRule(question);
    if (rule !== undefined) {
        return rule.nextId;
    } else {
        return question.defaultNextId;
    }
}

function getFirstAppliedRule(question) {
    return question.rules && question.rules.find(isRuleTrue);
}

function isRuleTrue(rule) {
    if (rule.type == "side-chain" && rule.alreadyTraversed) {
        return false;
    }
    rule.alreadyTraversed = true;

    if (rule.subRules) {
        if (rule.logic == "and")
            return rule.subRules.every(isRuleTrue);
        if (rule.logic == "or")
            return rule.subRules.find(isRuleTrue) != undefined;
    }

    let evaluation = true;
    let visitedNode;
    if (rule.logic == "visited node" && rule.visitedNode) {
        visitedNode = rule.visitedNode;
        evaluation = summary.isNodeVisited(visitedNode.nodeKey);
        if (visitedNode.not) {
            evaluation = !evaluation;
        }
        return evaluation;
    }

    let answerMap = summary.getMap();
    let condition = rule.condition;
    if (condition) {
        answerObj = answerMap[condition.questionId];
        // if (answerObj) {
        //     if (condition.operator) { // Operator
        //         let answer = parseInt(answerObj.answer.str);
        //         let operatorValue = parseInt(condition.operatorValue);
        //         if (condition.operator === ">") {
        //             evaluation = answer > operatorValue;
        //         } else if (condition.operator === ">=") {
        //             evaluation = answer >= operatorValue;
        //         } else if (condition.operator === "<") {
        //             evaluation = answer < operatorValue;
        //         } else if (condition.operator === "<=") {
        //             evaluation = answer <= operatorValue;
        //         } else if (condition.operator === "=") {
        //             evaluation = answer == operatorValue;
        //         }
        //     }
        //     else { // Regular answer condition
        //         if (answerObj.selectedAnswers) {
        //             evaluation = answerObj.selectedAnswers.find(a => {
        //                 return a.str === condition.answer;
        //             });
        //         } else {
        //             evaluation = answerObj.answer.str === condition.answer;
        //         }
        //     }
        // } else {
        //     evaluation = false;

        //     // let ruleOut = tree.getRuleOut(condition.questionId);
        //     // if (ruleOut) { // Found the rule-out
        //     //     if (ruleOut.score) {
        //     //         answerObj = {answer: {str: ruleOut.score.value}};
        //     //     }
        //     // }
        // }

        if (!answerObj) {
            evaluation = false;
            let ruleOut = tree.getRuleOut(condition.questionId);
            if (ruleOut) { // Found the rule-out - a value of a rule out is not the answer (because it doesn't exist), but the ruleout's score.
                if (ruleOut.score) {
                    let scoreValue;
                    if (ruleOut.score.value || ruleOut.score.value == 0) {
                        scoreValue = ruleOut.score.value;
                    }
                    answerObj = {answer: {str: scoreValue}};
                }
            }
        }

        let inputGroup = tree.getInput(condition.questionId);
        if (inputGroup) { // Found the inputGroup - the value will be the value field for a specific answer
            let parameter = inputGroup.answers.find(a => a.str === condition.answer);
            if (parameter) {
                if (parameter.type === 'checkbox') {
                    if (parameter.value) {
                        answerObj = {answer: {str: parameter.str}}; // Act as if the user has answered this answer.
                    }
                } else if (parameter.type === 'free-text' || parameter.type === 'radio') {
                    if (parameter.type == 'radio' && !parameter.value) {
                        // radio is not filled.
                    } else {
                        answerObj = {answer: {str: parameter.value}};
                    }
                } else {
                    console.log("ruleEvaluator: Unknown input parameter type: " + parameter.type);
                }
            }
        }

        evaluation = isConditionSatisfied(answerObj, condition, evaluation);

        if (!answerObj) { // If there's no answer - this is false.
        //     if (condition.not) // NOT something, and we answered empty.
        //         return true;
        //     else 
                return false;
        }
    }

    if (condition && condition.not) {
        evaluation = !evaluation;
    }

    return evaluation;
}

function isConditionSatisfied(answerObj, condition, evaluation) {
    if (!answerObj)
        return evaluation;

    if (condition.operator) { // Operator
        let answer = parseValueToInt(answerObj.answer.str);
        if (Number.isNaN(answer)) {
            answer = answerObj.answer.str;
        }
        let operatorValue = parseValueToInt(condition.operatorValue);
        if (Number.isNaN(operatorValue)) {
            operatorValue = condition.operatorValue;
        }
        if (condition.operator === ">") {
            evaluation = answer > operatorValue;
        } else if (condition.operator === ">=") {
            evaluation = answer >= operatorValue;
        } else if (condition.operator === "<") {
            evaluation = answer < operatorValue;
        } else if (condition.operator === "<=") {
            evaluation = answer <= operatorValue;
        } else if (condition.operator === "=") {
            evaluation = answer == operatorValue;

        }
    } else { // Regular answer condition
        if (answerObj.selectedAnswers) {
            evaluation = answerObj.selectedAnswers.find(a => {return a.selected && a.str === condition.answer}) != undefined;
        } else {
            evaluation = answerObj.answer.str === condition.answer;
        }
    }
    return evaluation;
}

function parseValueToInt(value) {
    let answer;
    try {
        answer = parseInt(value);
    } catch(err) {
        let index = value.indexOf("  -") // Just in case they added another space.. idk
        if (index == -1) {
            let index = value.indexOf(" -")
        }
        if (index == -1) {
            index = value.indexOf("-")
        }
        if (index != -1) {
            let trimmedAnswer = value.substring(0,index);
            answer = parseInt(trimmedAnswer);
        }
    }
    return answer;
}

module.exports = {getFirstAppliedRule, isRuleTrue}
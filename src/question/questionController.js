const angularModule = require('../angular/angularModule');
const summary = require('../summary/summary');
const ruleEvaluator = require('../ruleEvaluator');
const actionEvaluator = require('../actionEvaluator');
let tree = require('../tree/tree');
let medicalSummary = require('../summary/medicalSummary');
let consts = require('../consts');
angularModule.controller('questionController', questionController);
let sideChainNavigator = require('./sideChainNavigator');
let answerRetriever = require('../summary/answerRetriever');
const RuleOutProbability = require('../summary/RuleOutProbability');
const ruleOutSummary = require('../summary/ruleOutSummary');
const inputState = require('../inputs/inputState');
let settings = require('../settings');

let visitedQuestions = [];
function questionController($scope) {
    function ctor(){
        $scope.next = next;
        $scope.toggleSelection = toggleSelection;
        $scope.back = back;
        $scope.current = tree.getFirst();
        $scope.questionMode = consts.questionMode;
        $scope.printSummary = printSummary;
        $scope.showSummary = showSummary;
        $scope.showReasons = showReasons;
        $scope.showAnswersSummary = showAnswersSummary;
        $scope.showMultiAnswerSummary = showMultiAnswerSummary;
        $scope.showInputsDialog = showInputsDialog;
        $scope.showSettingsDialog = showSettingsDialog;
        $scope.getAnswersSummary = getAnswersSummary;
        $scope.hasRows = hasRows;
        $scope.isNextDisabled = isNextDisabled;
        $scope.setSelectionOnYesNo = setSelectionOnYesNo;
        $scope.getInputParameters = getInputParameters;
        $scope.onPressedSubmitInputDialog = onPressedSubmitInputDialog;
        $scope.closeModal = closeModal;
        $scope.getAppSettings = getAppSettings;
        $scope.shouldDisplayActionConfirmButton = shouldDisplayActionConfirmButton;
        $scope.getRadioValues = getRadioValues;
        $scope.getRadioTitle = getRadioTitle;
        $scope.getNgModelName = getNgModelName;
        $scope.performAction = performAction;
        $scope.getRuleOutSummary = getRuleOutSummary;
        $scope.shouldShowRuleOutSummary = shouldShowRuleOutSummary;
        $scope.visitedQuestions = visitedQuestions;
        visitedQuestions.push($scope.current.id);

        // Popups
        $scope.getRuleOutsPopupText = getRuleOutsPopupText;
        $scope.getActionsPopupText = getActionsPopupText;
        $scope.hideRuleOutsPopup = hideRuleOutsPopup;
        $scope.hideActionsPopup = hideActionsPopup;
        $scope.getItemData = getItemData;
        $scope.getItemName = getItemName;
        $scope.getItemScore = getItemScore;
        $scope.getStrengthTextClass = getStrengthTextClass;
        $scope.allQuestionsAnswers = allQuestionsAnswers;

        // Inputs
        $scope.inputParameters = inputState.get();
    }

    function setSelectionOnYesNo(answer, isYes) {
        let oldAnswer = answer.selected;
        if (isYes) {
            answer.selected = true;
        } else {
            answer.selected = false;
        }

        if (isYes != oldAnswer) {
            summary.append($scope.current, answer);
        }
        answer.isAnswered = true;
    }

    function toggleSelection(answer) {
        answer.selected = !answer.selected;
        summary.append($scope.current, answer);
    }

    function selectSingleAnswer(answer) {
        if (!answer || !answer.str)
            return;

        summary.append($scope.current, answer);
        $scope.current.answers.forEach(a => {
            a.selected = false;
        });
        answer.selected = true;
    }

    function getNextQuestionId(question, answer) {
        let firstAppliedRule;
        let nextId;
        if (answer) {
            answer = answer.nextId;
        }
        if (nextId === undefined) {
            firstAppliedRule = ruleEvaluator.getFirstAppliedRule(question);
        }
        if (firstAppliedRule) {
            nextId = firstAppliedRule.nextId;
            if (firstAppliedRule.type == "side-chain") {
                sideChainNavigator.setState(question);
                console.log("Starting side-chain from question: " + question.id + " to question " + nextId);
            }
        } else {
            nextId = $scope.current.defaultNextId;
        }
        return nextId;
    }

    function insertEmptyArrayForEmptyAnswer() {
        let answersMap = summary.getMap();
        if (!answersMap[$scope.current.id]) {
            summary.setEmptyAnswer($scope.current);
        }
    }

    // This function aligns the question.answers array (which holds all the answers, with a "selected" flag) with the answer.selectedQuestions array.
    // (In case we backed up and unselected an answer)
    function clearUnselectedAnswers() {
        let answer = summary.get($scope.current.id);

        let selectedAnswersToRemove = [];
        if (answer && answer.question && answer.question.answers && answer.selectedAnswers) {
            answer.question.answers.forEach(actualAnswer => {
                let selectedAnswer = answer.selectedAnswers.find(a => actualAnswer.str === a.str)
                if (selectedAnswer && !actualAnswer.selected) {
                    console.log("Found an answer in selectedAnswers, that is not selected in the actual question answers.");
                    selectedAnswer.selected = false;
                    selectedAnswersToRemove.push(selectedAnswer);
                }
            });
        }

        while (selectedAnswersToRemove.length>0) {
            let toRemove = selectedAnswersToRemove[0];
            let index = answer.selectedAnswers.indexOf(toRemove);
            if (index != -1) {
                if (toRemove) {
                    console.log("Removing from selectedAnswers: " + toRemove.str);
                }
                answer.selectedAnswers.splice(index, 1);
            }
            selectedAnswersToRemove.splice(0, 1);
        }
    }

    function next(answer) {
        selectSingleAnswer(answer);
        clearUnselectedAnswers();
        insertEmptyArrayForEmptyAnswer();

        let nextId = getNextQuestionId($scope.current, answer);

        // Apply the new state
        applyRuleOuts($scope.current);
        summary.setVisitedNode($scope.current.id, true); // This should be before I guess?

        // Save a snapshot of the state of this question
        if (visitedQuestions.indexOf($scope.current.id) == visitedQuestions.length-1) { // Only save if we haven't been here (if the first (and only) occurence of this question is the last index.) - for double-nexts when we finish side-chains
            let answers = summary.get($scope.current.id);
            if (answers) { // Could be that the user pressed "Skip" or answered an empty free-text area
                if (answers.answer) {
                    answers = answers.answer;
                } else if (answers.selectedAnswers) {
                    answers = [...new Set(answers.selectedAnswers)];
                }

                questionStates[$scope.current.id] = {actions: [...new Set(summary.getRuleOuts())], visitedNodes: [...new Set(summary.getVisitedNodes())], answers: answers};
            }
        }

        let movedToNext = true;
        if (tree.getQuestion(nextId)) {
            $scope.current = tree.getQuestion(nextId);
            $scope.end = $scope.current.end;
            lastAppliedRuleOuts = [];
        } else {
            let sideChainStartQuestion = sideChainNavigator.getState();
            if (!sideChainStartQuestion && $scope.current.baseQuestionId) { // This is for when we back to the end of a side-chain
                sideChainStartQuestion = tree.getQuestion($scope.current.baseQuestionId);
            }

            if (sideChainStartQuestion) {
                $scope.current.baseQuestionId = sideChainStartQuestion.id;
                $scope.current = sideChainStartQuestion;
                $scope.end = $scope.current.end;
                lastAppliedRuleOuts = [];

                console.log("Finished side-chain from question: " + sideChainStartQuestion.id);
                sideChainNavigator.setState(null);
                next(answerRetriever.getAnswer(sideChainStartQuestion.id));
            } else {
                alert("Graph reached a dead end");
                movedToNext = false;
            }
        }
        hideRuleOutsPopup(false);
        hideActionsPopup(false);
        if (movedToNext) {
            if (!visitedQuestions.includes($scope.current.id)) { // If we've been here, this is a side-chain ending. No need to add the base question again.
                visitedQuestions.push($scope.current.id);
            }
        }

        let currentQuestionSnapshot = questionStates[$scope.current.id];
        if (currentQuestionSnapshot) {
            summary.restoreAnswers($scope.current, currentQuestionSnapshot.answers);
        }
    }

    let questionStates = {}; // For every question, a snapshots of the ruleouts in that point. (Stored AFTER answering the questions, so it's inclusive to that question)

    let lastAppliedRuleOuts = [];
    function applyRuleOuts(question) {
        let appliedRuleOuts = evaluateActionsRecursively(question);
        if (!appliedRuleOuts || appliedRuleOuts.length==0)
            return;

        allRuleOutsTexts = "";
        allActionsTexts = "";
        lastAppliedRuleOuts = [];
        let delimiter = "\n";

        // TODO: This is stupid, we do this twice?
        appliedRuleOuts.forEach(ro => {
            let roId = ro.ruleOutId;
            let roReasons = ro.reasons;
            let ruleOut = tree.getRuleOut(roId);
            if (!ruleOut) // TODO: Again, this is a case where an action eventually redirects back to a question
                return;

            if (ruleOut["reasons"]) {
                for (let index in ruleOut.reasons) { // Remove the old reasons from this question (if we went back and changed the answer)
                    let oldReason = ruleOut.reasons[index];
                    if (oldReason.condition && oldReason.condition.questionId == $scope.current.id && visitedQuestions[visitedQuestions.length-1] == oldReason.condition.questionId) { // if the old reason is from this question, and it's the last question we visited (which doesn't happen on double-next (side-chain), but does happen when pressing back and then changing answers)
                        console.log('Removing reason: ' + oldReason.condition.answer);
                        ruleOut.reasons.splice(index, 1);
                        // let roReason = roReasons.find(r => r.condition && r.condition.questionId === oldReason.condition.questionId); // Get the same reason in the roReasons array
                        // if (roReason) {
                        //     let roReasonIndex = roReasons.indexOf(roReason);
                        //     if (roReasonIndex != -1) {
                        //         roReasons.splice(roReasonIndex, 1);
                        //     }
                        // }
                    }
                }
            }
        });

        appliedRuleOuts.forEach(ro => {
            let roId = ro.ruleOutId;
            let roReasons = ro.reasons;
            let ruleOut = tree.getRuleOut(roId);
            if (!ruleOut) // TODO: Again, this is a case where an action eventually redirects back to a question
                return;

            if (ruleOut["reasons"]) {
                roReasons.forEach(reason => ruleOut["reasons"].push(reason));
                checkForSameQuestionDuplicates(ruleOut["reasons"]);
            } else {
                ruleOut["reasons"] = roReasons;
            }
            ruleOut["reasons"] = eliminateDuplicateReasons(ruleOut["reasons"]);
            if (ro.isApplied) { // Only if the rule is true. This is done for when we have a R/O rule: {and: [x,y]} but only x isTrue - we want to add x as a reason, but not to add the ruleout itself, because the GLOBAL rule doesn't apply.
                lastAppliedRuleOuts.push(ruleOut);
            }
        });

        removeNonMustRuleOuts(lastAppliedRuleOuts);

        lastAppliedRuleOuts = [...new Set(lastAppliedRuleOuts)];
        lastAppliedRuleOuts.forEach(ruleOut => {
            // Only if this is the first time adding the action/rule-out
            let previousActions = questionStates[getQuestionBeforePreviousId()]; // The state before answering this question
            if (previousActions && previousActions.visitedNodes && previousActions.visitedNodes.includes(ruleOut.id))
                return;

            if (ruleOut)
            if (ruleOut.mode === "rule-out") {
                if(ruleOut.alert) {
                    ruleOut["type"] = "alert";
                } else {
                    ruleOut["type"] = "rule-out";
                    allRuleOutsTexts += ruleOut.questionStr + delimiter;
                } 
            } 
            else if (ruleOut.mode === "action") {
                ruleOut["type"] = "action";
                let message = ruleOut.message;
                allActionsTexts += message + delimiter;
            }
        });

        if (allRuleOutsTexts != "") {
            allRuleOutsTexts = allRuleOutsTexts.substring(0, allRuleOutsTexts.length-(delimiter.length));
            showRuleOutsPopup();
        }
        if (allActionsTexts != "") {
            let allRows = allActionsTexts.split(delimiter);
            allRows = [...new Set(allRows)];
            allActionsTexts = "";
            allRows.forEach(r => allActionsTexts += r+delimiter);
            allActionsTexts = allActionsTexts.substring(0, allActionsTexts.length-(delimiter.length));
            showActionsPopup();
        }
        // eliminateThisQuestionsLastAppliedRuleOuts(lastAppliedRuleOuts);
        summary.addRuleOuts(lastAppliedRuleOuts);
    }


    function removeRuleOutReasons(ruleOutId, rule) {
        if (rule.condition) {
            let ruleOut = tree.getRuleOut(ruleOutId);
            if (!ruleOut.reasons)
                return; // This rule out isn't applied

            let relevantReason = ruleOut.reasons.find(reason => reason == rule);
            if (relevantReason) {
                let index = ruleOut.reasons.indexOf(relevantReason);
                if (index != -1) {
                    ruleOut.reasons.splice(index, 1);
                }
            }

            if (ruleOut.reasons.length == 0) {
                summary.removeRuleOut(ruleOut);
                summary.setVisitedNode(ruleOutId, false);
            }

        } else if (rule.subRules) {
            rule.subRules.forEach(sr => removeRuleOutReasons(ruleOutId, sr));
        }
    }


    function applyRuleOutsForMultipleQuestions(questions, showPopups) {
        let delimiter = "\n";
        let ruleOutTexts = [];
        let actionsTexts = [];

        for (let key in questions) {
            let question = questions[key];

            // TODO: This should actually happen recursively... what if we remove a R.O that has a default rule to an action? we should remove the action as well

            // First, remove the ruleouts created & reasons by this question. we will re-evaluate it here.
            question.ruleOutRules.forEach(rule => removeRuleOutReasons(rule.nextId, rule));

            let appliedRuleOuts = evaluateActionsRecursively(question);
            if (!appliedRuleOuts || appliedRuleOuts.length==0)
                continue;
    
            // allRuleOutsTexts = "";
            // allActionsTexts = "";
            // lastAppliedRuleOuts = [];
            // let delimiter = "\n";
    
            // TODO: This is stupid, we do this twice?
            appliedRuleOuts.forEach(ro => {
                let roId = ro.ruleOutId;
                let roReasons = ro.reasons;
                let ruleOut = tree.getRuleOut(roId);
                if (!ruleOut) // TODO: Again, this is a case where an action eventually redirects back to a question
                    return;
    
                if (ruleOut["reasons"]) {
                    for (let index in ruleOut.reasons) { // Remove the old reasons from this question (if we went back and changed the answer)
                        let oldReason = ruleOut.reasons[index];
                        if (oldReason.condition && oldReason.condition.questionId == $scope.current.id && visitedQuestions[visitedQuestions.length-1] == oldReason.condition.questionId) { // if the old reason is from this question, and it's the last question we visited (which doesn't happen on double-next (side-chain), but does happen when pressing back and then changing answers)
                            console.log('Removing reason: ' + oldReason.condition.answer);
                            ruleOut.reasons.splice(index, 1);
                        }
                    }
                }
            });
    
            appliedRuleOuts.forEach(ro => {
                let roId = ro.ruleOutId;
                let roReasons = ro.reasons;
                let ruleOut = tree.getRuleOut(roId);
                if (!ruleOut) // TODO: Again, this is a case where an action eventually redirects back to a question
                    return;
    
                if (ruleOut["reasons"]) {
                    roReasons.forEach(reason => ruleOut["reasons"].push(reason));
                    checkForSameQuestionDuplicates(ruleOut["reasons"]);
                } else {
                    ruleOut["reasons"] = roReasons;
                }
                ruleOut["reasons"] = eliminateDuplicateReasons(ruleOut["reasons"]);
                if (ro.isApplied) { // Only if the rule is true. This is done for when we have a R/O rule: {and: [x,y]} but only x isTrue - we want to add x as a reason, but not to add the ruleout itself, because the GLOBAL rule doesn't apply.
                    lastAppliedRuleOuts.push(ruleOut);
                }
            });
    
            removeNonMustRuleOuts(lastAppliedRuleOuts);
    
            lastAppliedRuleOuts = [...new Set(lastAppliedRuleOuts)];
            lastAppliedRuleOuts.forEach(ruleOut => {
                // Only if this is the first time adding the action/rule-out
                let previousActions = questionStates[getQuestionBeforePreviousId()]; // The state before answering this question
                if (previousActions && previousActions.visitedNodes && previousActions.visitedNodes.includes(ruleOut.id))
                    return;
    
                // TODO: THE FUUCK IS THIS
                if (ruleOut)
                if (ruleOut.mode === "rule-out") {
                    // allRuleOutsTexts += ruleOut.questionStr + delimiter;
                    if(ruleOut.alert) {
                        ruleOut["type"] = "alert";
                    }
                    else {
                        ruleOut["type"] = "rule-out";
                        ruleOutTexts.push(ruleOut.questionStr);
                        // TODO: Do we want a popup for alerts?
                        // ruleOutTexts.push(ruleOut.questionStr);
                    } 
                } else if (ruleOut.mode === "action") {
                    ruleOut["type"] = "action";
                    let message = ruleOut.questionStr;
                    if (ruleOut.message) {
                        message = ruleOut.message;
                    }
                    // allActionsTexts += message + delimiter;
                    actionsTexts.push(message);
                }
            });
        };

        if (showPopups) {
            ruleOutTexts = [...new Set(ruleOutTexts)];
            ruleOutTexts.forEach(t => allRuleOutsTexts += t+delimiter);
            actionsTexts = [...new Set(actionsTexts)];
            actionsTexts.forEach(t => allActionsTexts += t+delimiter);
            if (allRuleOutsTexts != "") {
                allRuleOutsTexts = allRuleOutsTexts.substring(0, allRuleOutsTexts.length-(delimiter.length));
                showRuleOutsPopup();
            } else {
                hideRuleOutsPopup();
            }
            if (allActionsTexts != "") {
                allActionsTexts = allActionsTexts.substring(0, allActionsTexts.length-(delimiter.length));
                showActionsPopup();
            } else {
                hideActionsPopup();
            }
        }
        summary.addRuleOuts(lastAppliedRuleOuts);
    }

    function removeNonMustRuleOuts(ruleOuts) {
        let ruleOutsToRemove = [];
        ruleOuts.forEach(ro => {
            if (ro.mode === 'rule-out' || ro.type === 'rule-out' || ro.mode == 'alert' || ro.type == 'alert') { // Dafuq?
                if (!ro.reasons) {
                    ruleOutsToRemove.push(ro);
                }
                else {
                    let foundMust = false;
                    ro.reasons.forEach(reason => {
                        // TODO: Should we check the subrule strength as well? where can the "strength" parameter be?
                        if (reason.strength === RuleOutProbability.RuleOutProbability.MUST) {
                            foundMust = true;
                        }
                    });

                    if (!foundMust) {
                        ruleOutsToRemove.push(ro);
                    }
                }
            } else {
                // console.log("guh");
            }
        });

        ruleOutsToRemove.forEach(toRemove => {
            let index = ruleOuts.indexOf(toRemove);
            if (index != -1) {
                ruleOuts.splice(index, 1);
            }
        });
    }

    /**
     * This function is called when we back up and change our answer, and both answers (old and new) were a reason for a R/O.
     * This function removes the old reason.
     */
    function checkForSameQuestionDuplicates(reasons) {
        let questionIdToReasonsMap = {};
        // reasons = [...new Set(reasons)];
        reasons.sort(function(a, b) { // Sort so that the reasons that have strength will be first
            if (a.strength || a.strength==0) {
                return b.strength || b.strength==0? 0 : -1;
            } 
            else if (b.strength || b.strength==0) {
                return a.strength || a.strength==0? 0 : 1;
            }
            return 0;
        });
        reasons.sort(function(a, b) { // After sorting so that the reasons with strength are first, sort in descending order of strength (so that if we have MUST and STRONG on the same condition, it will first set MUST)
            let aStrength = 0;
            let bStrength = 0;
            if (a.strength)
                aStrength = a.strength;
            if (b.strength)
                bStrength = b.strength;
    
            return bStrength - aStrength;
        });

        
        reasons.forEach(reason => {
            if (reason.condition) {
                let questionKey = reason.condition.questionId;
                if (tree.getInput(questionKey)) { // If it's an Input, the key is the parameter itself, and not the question (which is a parameter group)
                    questionKey = reason.condition.questionId+"_"+reason.condition.answer;
                }

                if (!questionIdToReasonsMap[questionKey]) {
                    questionIdToReasonsMap[questionKey] = [];
                } 
                questionIdToReasonsMap[questionKey].push(reason);
            }
        });

        for (let key in questionIdToReasonsMap) {
            let questionReasons = questionIdToReasonsMap[key];
            if (!questionReasons)
                continue;

            // if (tree.getInput(key))
            //     continue; // Don't remove duplicates for inputs?

            let actualAnswers = summary.get(key);
            if (actualAnswers) {
                let reasonsToRemove = [];
                if (actualAnswers.selectedAnswers) {
                    questionReasons.forEach(reasonAnswer => {
                        let found = false;
                        actualAnswers.selectedAnswers.forEach(actualAnswer => {
                            if (!reasonAnswer.condition.not && actualAnswer.str === reasonAnswer.condition.answer ||
                                reasonAnswer.condition.not && actualAnswer.str !== reasonAnswer.condition.answer) { // Looking for the answer that is consistent with this reason.
                                found = true;
                            }
                        });

                        if (!found) {
                            let isEmptyAnswerAndNotCondition = (actualAnswers.selectedAnswers.length == 0 && reasonAnswer.condition.not)
                            if (!isEmptyAnswerAndNotCondition) {
                                // console.log("FOUND A DUPLICATE ARRAY-ARRAY: " + reasonAnswer);
                                reasonsToRemove.push(reasonAnswer);
                            }
                        }
                    });
                } else {
                    questionReasons.forEach(reasonAnswer => {
                        if (actualAnswers.answer && reasonAnswer.condition && 
                            ((!reasonAnswer.condition.not && actualAnswers.answer.str !== reasonAnswer.condition.answer) ||
                            (reasonAnswer.condition.not && actualAnswers.answer.str === reasonAnswer.condition.answer))) { // Looking for a reason that isn't consistent with our answers
                            // console.log("FOUND A DUPLICATE THAT SHOULDNT BE HERE!!!!!!!!! VALUE-VALUE: " + reasonAnswer.condition.answer);
                            reasonsToRemove.push(reasonAnswer);
                        }
                    });
                }

                reasonsToRemove.forEach(toRemove => {
                    let index = reasons.indexOf(toRemove);
                    if (index != -1) {
                        reasons.splice(index, 1);
                    }
                });
            }
        }
    }

    function eliminateDuplicateReasons(reasons) {
        let uniqueReasons = [];
        reasons = [...new Set(reasons)];
        reasons.sort(function(a, b) { // Sort so that the reasons that have strength will be first
            if (a.strength || a.strength==0) {
                return b.strength || b.strength==0? 0 : -1;
            } 
            else if (b.strength || b.strength==0) {
                return a.strength || a.strength==0? 0 : 1;
            }
            return 0;
        });
        reasons.sort(function(a, b) { // After sorting so that the reasons with strength are first, sort in descending order of strength (so that if we have MUST and STRONG on the same condition, it will first set MUST)
            let aStrength = 0;
            let bStrength = 0;
            if (a.strength)
                aStrength = a.strength;
            if (b.strength)
                bStrength = b.strength;
    
            return bStrength - aStrength;
        });
        reasons.forEach((r) => {
            if (!uniqueReasons.find(unique => isSameReason(unique, r))) {
                uniqueReasons.push(r);
            }
        });
        return uniqueReasons;
    }

    function isSameReason(a, b) {
        return a.condition && b.condition && a.condition.questionId === b.condition.questionId && a.condition.answer === b.condition.answer
    }


    function evaluateActionsRecursively(startingQuestion) {
        let appliedActionsIds = actionEvaluator.evaluateActions(startingQuestion);
        for (index in appliedActionsIds) {
            let a = appliedActionsIds[index];
            if (!a)
                continue;

            let actionId = a.ruleOutId;

            // TODO: Check if we already have this R/O. if we do, add the reasons. I guess this will cause a bug when we go back and remove the MUST.. it will still show probably
            // let isMust = summary.isNodeVisited(actionId);
            let isMust = false;
            let isAction = false;
            let isRuleOut = false;
            let action = tree.getRuleOut(actionId);

            if (!action) { // TODO: This is a case where an action eventually redirects back to a question
                console.log("Action not found in evaluateActionsRecursively?!");
                continue;
            }

            if (action.type == "rule-out" || action.mode == "rule-out" || action.type == "alert" || action.mode == "alert") { // TODO: dafuq?
                isRuleOut = true;
                if (a.reasons) {
                    a.reasons.forEach(reason => {
                        if (reason.strength == RuleOutProbability.RuleOutProbability.MUST) {
                            isMust = true;
                        }
                    });
                }
            } else if (action.type == "action" || action.mode == "action") {
                isAction = true;
            }

            if (a.isApplied && (isMust || isAction)) {
                summary.setVisitedNode(actionId, true); // Visited the outer-nodes
                if (isRuleOut) {
                    let ruleOutScore = calculateRuleOutScore(action);
                    if ((ruleOutScore || ruleOutScore==0) && action.score) {
                        action.score.value = ruleOutScore;
                    }
                }

                let innerActionIds = evaluateActionsRecursively(action);
                innerActionIds.forEach(innerActionId => {
                    appliedActionsIds.push(innerActionId);
                    summary.setVisitedNode(innerActionId.ruleOutId, true); // Visited the nested nodes
                });
            }
            // else if (!a.isApplied && (isMust)) { // When there's a must, but the must 
            //     let innerActionIds = evaluateActionsRecursively(action);
            //     innerActionIds.forEach(innerActionId => {
            //         appliedActionsIds.push(innerActionId);
            //         // TODO: ?
            //         // summary.setVisitedNode(innerActionId.ruleOutId, true); // Visited the nested nodes
            //     });
            // }
        }
        return appliedActionsIds;
    }

    // TODO: This is a duplicate of the function in questionController... make this some module
    function calculateRuleOutScore(ruleOut) {
        if (!ruleOut.score || !ruleOut.score.values || ruleOut.score.values.length == 0)
            return;

        let score = 0;
        ruleOut.score.values.forEach(v => {
            if (!v.rule || !v.value)
                return;
            
            if (ruleEvaluator.isRuleTrue(v.rule)) {
                score += v.value;
            }
        });
        return score;
    }

    function showRuleOutsPopup() {
        if (!settings.getShowRuleOutsPopup())
            return;

        let popup = document.getElementById("ruleOutsPopup");
        showPopup(popup, allRuleOutsTexts);
    }

    function showActionsPopup() {
        let popup = document.getElementById("actionsPopup");
        showPopup(popup, allActionsTexts);
    }

    function hideRuleOutsPopup(forceHide) {
        let popup = document.getElementById("ruleOutsPopup");
        hidePopup(popup, forceHide, allRuleOutsTexts);
    }

    function hideActionsPopup(forceHide) {
        let popup = document.getElementById("actionsPopup");
        hidePopup(popup, forceHide, allActionsTexts);
    }

    function showPopup(popup, popupText) {
        let isShowing = false;
        for (let itemIndex in popup.classList) {
            if (popup.classList[itemIndex] === "show") {
                isShowing = true;
            }
        }
        if (!isShowing && popupText && popupText != "") {
            popup.classList.toggle("show");
        }

    }

    function hidePopup(popup, forceHide, popupText) {
        let isShowing = false;
        for (let itemIndex in popup.classList) {
            if (popup.classList[itemIndex] === "show") {
                isShowing = true;
            }
        }
        if (isShowing) {
            if (!popupText || popupText == "" || forceHide) {
                popup.classList.toggle("show");
            }
        }
    }

    let allRuleOutsTexts = "";
    let allActionsTexts = "";
    function getRuleOutsPopupText() {
        return allRuleOutsTexts;
    }
    function getActionsPopupText() {
        return allActionsTexts;
    }

    function back() {
        visitedQuestions.pop();
        let previousQuestionId = getPreviousQuestionId();

        if (previousQuestionId !== undefined) {
            // if (questionStates[$scope.current.id]) { // Clear the state
            //     questionStates[$scope.current.id] = null;
            //     delete questionStates[$scope.current.id];
            // }
            summary.clearAnswers($scope.current); // TODO: This makes the user type in everything again..
            $scope.current = tree.getQuestion(previousQuestionId);
            $scope.end = $scope.current.end;
            $scope.summary = null;
            summary.setVisitedNode($scope.current.id, false);

            if ($scope.current.baseQuestionId) {
                console.log("Backed into a side-chain");
                let baseQuestion = tree.getQuestion($scope.current.baseQuestionId);
                sideChainNavigator.setState(baseQuestion);
            }

            let baseQuestion = sideChainNavigator.getState();
            if (baseQuestion) {
                baseQuestion.clearTraversedSideChainsWithHistory(visitedQuestions); // When we're backing-out from a question in the side-chain, clear the base question's traversed side chains values
                $scope.current.baseQuestionId = baseQuestion.id; // Set the base quesiton
            } else {
                $scope.current.clearTraversedSideChains(); // Set alreadyTraversed=false on all the side-chain rules of this question - we backed out
            }


            let beforePreviousSnapshotIndex = getQuestionBeforePreviousId(); // We backed into this question - restore its snapshot (which is essentially the snapshot after answer the previous question of the question we just backed-out into)
            if (beforePreviousSnapshotIndex) {
                let beforePreviousSnapshot = questionStates[beforePreviousSnapshotIndex];
                if (beforePreviousSnapshot) {
                    summary.restoreSnapshot({actions:beforePreviousSnapshot.actions, visitedNodes:beforePreviousSnapshot.visitedNodes});
                }
            } else { // We have backed-out into the first question
                summary.restoreSnapshot({}); // TODO: Is this ok? just override everything?
            }
            submitInputsWithoutPopups(); // Restoring the snapshot will override the visitedNodes & actions that came from answering Inputs in the middle of the questionere. so run all the input rules again after this (and don't show the popups... derp)

            // Clean & restore selected answers.
            // summary.clearAnswers($scope.current);
        }
    }

    function getPreviousQuestionId() {
        if (visitedQuestions.length == 0)
            return "0";

        return visitedQuestions[visitedQuestions.length-1]; // Peek
    }

    function getQuestionBeforePreviousId() {
        if (visitedQuestions.length <= 1)
            return;

        return visitedQuestions[visitedQuestions.length-2]; // Peek
    }

    function performAction(item) {
        let allAppliedRuleOuts = summary.getRuleOuts();
        if (!allAppliedRuleOuts)
            return;

        let action = allAppliedRuleOuts.find(a => a.id == item.id);
        if (!action)
            return;

        action.isConfirmed = true;
        generateSummary();
    }

    function printSummary() {
        generateSummary();
    }

    function generateSummary() {
        $scope.summary = medicalSummary.generate();
    }

    function showSummary() {
        generateSummary();
        showModal("myModal");
    }

    function showInputsDialog() {
        showModal("inputs-modal", submitInputsWithPopups);
    }

    function showSettingsDialog() {
        showModal("settings-modal");
    }

    let selectedItem;
    function getItemData() {
        if (selectedItem && selectedItem.reasons && selectedItem.reasons.length > 0) {
            return selectedItem.reasons.sort(function(r1,r2) {
                console.log(r1);
            });
        }
    }
    function getItemName() {
        if (selectedItem && selectedItem.name)
            return selectedItem.name;
        else
            return "";
    }
    function getItemScore() {
        if (selectedItem && (selectedItem.score || selectedItem.score==0)) {
            return selectedItem.score;
        }
        return null;
    }

    function getRuleOutSummary() {
        if (shouldShowRuleOutSummary()) {
            return ruleOutSummary.generate(selectedItem.originalNode);
        }
        return null;
    }

    function shouldShowRuleOutSummary() {
        return selectedItem && selectedItem.originalNode.ruleOutText;
    }

    function getStrengthTextClass(item) {
        let strengthText = item.strengthText;
        if (!strengthText)
            return "";

        switch (strengthText) {
            case RuleOutProbability.WEAK_TEXT:
                return "weak-strength-text";
            case RuleOutProbability.STRONG_TEXT:
                return "strong-strength-text";
            case RuleOutProbability.VERY_STRONG_TEXT:
                return "very-strong-strength-text";
            case RuleOutProbability.MUST_TEXT:
                return "must-strength-text";
        }
    }
    
    function showReasons(action) {
        selectedItem = action;
        showModal("item-modal");
    }

    function hasRows(paragraph) {
        if (!paragraph.rows)
            return false;
        
        let foundNonEmptyRow = false;
        paragraph.rows.forEach(r => {
            if (r && r.length>0) {
                foundNonEmptyRow = true;
            }
        });
        return foundNonEmptyRow;
    }

    let allQuestionsAnswers = [];
    function showAnswersSummary() {
        allQuestionsAnswers = getAllQuestions();
        showModal("answers-summary-modal");
    }
    function showMultiAnswerSummary(question) {
        $scope.selectedMultiAnswerQuestion = question;
        showModal("multi-answer-summary-modal");
    }

    function getAnswersSummary() {
        return allQuestionsAnswers;
    }

    function getAllQuestions() {
        let questionToAnswerMap = [];
        let questions = tree.getAllQuestions();
        for (let key in questions) {
            let q = questions[key];
            let a = answerRetriever.getAnswer(key);
            let questionToPush;
            if (!a) // TODO: This will continue for answers that we have not answered yet, but will also continue for empty answers..
                continue;

            if (Array.isArray(a)) {
                let answers = [];
                let unselectedAnswers = [];

                // All answers
                q.answers.forEach(answer => {
                    if (answer && answer.str) {
                        if (answer.selected) {
                            answers.push(answer.str);
                        } else {
                            unselectedAnswers.push(answer.str);
                        }
                    }
                });

                // if (answers.length > 0) {
                    questionToPush = {answers: answers, unselectedAnswers: unselectedAnswers};
                // }
            } else {
                questionToPush = {answer: a};
            }
            questionToPush.questionStr = q.questionStr;
            questionToPush.doctorDisplayText = q.doctorDisplayText;
            questionToPush.shouldDisplayForDoctor = q.shouldDisplayForDoctor;
            questionToAnswerMap.push(questionToPush);
        }
        return questionToAnswerMap
    }

    function showModal(modalId, onCloseCallback) {
        var modal = document.getElementById(modalId);
        var span = document.getElementsByClassName(modalId+"-close")[0];
        modal.style.display = "block";

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.display = "none";
            if (onCloseCallback) {
                onCloseCallback();
            }
        }

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
                if (onCloseCallback) {
                    onCloseCallback();
                }
            }
        }
    }

    function isNextDisabled() {
        let currentQuestion = $scope.current;
        if (!currentQuestion)
            return false;

        if (currentQuestion.mode==consts.questionMode.MULTI_CHOICE && currentQuestion.multiChoiceYesNo) {
            return $scope.current.answers.find(a => {
                return !a.isAnswered;
            });
        }
        return false;
    }

    inputWeight = {};
    inputWeight["-39"] = 100;
    inputWeight["-51"] = 99;
    inputWeight["-43"] = 98;
    inputWeight["-64"] = 97;
    inputWeight["-47"] = 96;
    inputWeight["-53"] = 95;
    inputWeight["-100"] = 94;

    function getInputParameters() {
        let allInputs = tree.getAllInputs();
        for (let key in allInputs) {
            parameterGroup = allInputs[key];
            parameterGroup.answers.forEach(parameter => {
                let ngModelName = getNgModelName(parameterGroup, parameter);
                if (!$scope.inputParameters[ngModelName]) {
                    if (parameter.type === "free-text") {
                        $scope.inputParameters[ngModelName] = "";
                    } else if (parameter.type === "checkbox") {
                        $scope.inputParameters[ngModelName] = false;
                    }else if (parameter.type === "radio") {
                        $scope.inputParameters[ngModelName] = undefined;
                    }
                }
            });
        };

        let orderedInputs = Object.values(allInputs).sort((a,b) => {
            a_weight = inputWeight[a.id] || 0;
            b_weight = inputWeight[b.id] || 0;
            return b_weight - a_weight;
        });
        return orderedInputs;
    }

    function getNgModelName(parameterGroup, parameter) {
        let ngModelName = parameterGroup.questionStr+"@@@_@@@"+parameter.str;
        return ngModelName;
    }

    function onPressedSubmitInputDialog() {
        closeModal("inputs-modal");
        submitInputsWithPopups();
    }

    function closeModal(modalId) {
        var modal = document.getElementById(modalId);
        modal.style.display = "none";
    }

    function submitInputsWithPopups() {
        submitInputs(true);
    }

    function submitInputsWithoutPopups() {
        submitInputs(false);
    }

    function submitInputs(showPopups) {
        let allInputs = tree.getAllInputs();
        for (let key in allInputs) {
            parameterGroup = allInputs[key];
            parameterGroup.answers.forEach(parameter => {
                let ngModelName = getNgModelName(parameterGroup, parameter);
                let paramValue = $scope.inputParameters[ngModelName];
                if (parameter.type === 'checkbox') {
                    if (paramValue == "")
                        paramValue = false;

                    if (paramValue || paramValue == false) {
                        parameter.value = paramValue;
                    }
                } else if (parameter.type === 'free-text') {
                    if (paramValue) {
                        parameter.value = paramValue;
                    }
                } else if (parameter.type === 'radio') {
                    if (paramValue) {
                        parameter.value = paramValue;
                    }
                } else {
                    console.log("unknown input parameter type: " + parameter.type);
                }
            });
        };

        // After setting all the values, run the rules
        if (showPopups) {
            allRuleOutsTexts = "";
            allActionsTexts = "";
            lastAppliedRuleOuts = [];
        }
        applyRuleOutsForMultipleQuestions(allInputs, showPopups);
        // if (showPopups) {
        //     $scope.$apply(); // Refresh the screen because it doesn't get the change
        // }
    }

    function getAppSettings() {
        return settings.getSettingsObject();
    }

    function shouldDisplayActionConfirmButton(action) {
        return !settings.getCruiseControl();
    }

    const RADIO_TITLE_DELIMITER = "::";
    const RADIO_VALUES_DELIMITER = ";";
    function getRadioTitle(parameterString) {
        let endIndex = parameterString.indexOf(RADIO_TITLE_DELIMITER);
        if (endIndex != -1) {
            return parameterString.substring(0, endIndex);
        }
    }

    function getRadioValues(parameterString) {
        let valuesStartIndex = parameterString.indexOf(RADIO_TITLE_DELIMITER);
        let valuesString = "";
        if (valuesStartIndex != -1) {
            valuesString = parameterString.substring(valuesStartIndex+RADIO_TITLE_DELIMITER.length);
        }

        return valuesString.split(RADIO_VALUES_DELIMITER);
    }
    
    ctor();
}
const questionTemplate = require('./question.html');
const angularModule = require('../angular/angularModule');
angularModule.directive('question', questionDirective);

function questionDirective() {
    return {
        restrict: 'E',
        transclude: true,
        scope: {},
        template: questionTemplate,
        controller: 'questionController',
        controllerAs: 'questionController'
    }
}
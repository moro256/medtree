const Question = require('../question/question');
const consts = require('../consts');

let map = {};

function createTree() {
    createQuestions(graph);
    createLinks(graph);
    return map;
}

function createQuestions(graphObj) {
    graphObj.nodeDataArray.forEach(createQuestion);
}

function createLinks(graphObj) {
    graphObj.linkDataArray.forEach(link => {
        let question = map[link.from];
        if (link.logic == "default") {
            question.defaultNextId = link.to;
        } else {
            let subRules = link.subRules && JSON.parse(link.subRules);
            question.addRule({logic:link.logic, nextId:link.to, subRules});
        }
    });
}

function createQuestion(node) {
    let answersRaw = JSON.parse(node.answers);
    let answers = answersRaw.map(rawAnswer => {
        return {str:rawAnswer}
    });

    let question = new Question(node.key, node.text, {answers, allowSkip: node.allowSkip, mode: node.mode});
    map[node.key] = question;
    return question;
}

graph = { "class": "GraphLinksModel",
"modelData": {"test":true, "hello":"world", "version":42},
"nodeDataArray": [ 
    {"text":"חווה תאונת דרכים, נפילה או חבלה?", "key":1, "answers":"[\"לא\",\"כן\"]", "allowSkip":true, "mode":"buttons", "color":"#B2DFDB", "loc":"949.4052734375 -77.25380859375"},
    {"text":"ידוע רקע של סכרת?", "key":2, "answers":"[\"לא\",\"כן\"]", "allowSkip":true, "mode":"buttons", "color":"#B2DFDB", "loc":"1082.88916015625 482.74619140625"},
    {"text":"יש רעידות?", "key":3, "answers":"[\"לא\",\"כן\"]", "allowSkip":true, "mode":"buttons", "color":"#B2DFDB", "loc":"533.36181640625 17.74619140625"},
    {"text":" האם ידוע על אלכוהליזם?", "key":4, "answers":"[\"לא\",\"כן\"]", "allowSkip":true, "mode":"buttons", "color":"#B2DFDB", "loc":"703.37744140625 17.74619140625"},
    {"text":"שתיית אלכוהול או שימוש בסמים?", "key":5, "answers":"[\"לא\",\"כן\"]", "allowSkip":true, "mode":"buttons", "color":"#B2DFDB", "loc":"954.4677734375 17.74619140625"},
    {"text":"מחלת כליות כרונית?", "key":6, "answers":"[\"לא\",\"כן\"]", "allowSkip":true, "mode":"buttons", "color":"#B2DFDB", "loc":"1189.57958984375 17.74619140625"},
    {"text":"האם יש כאב בטן, עצירות, בחילות או הקאות?", "key":7, "answers":"[\"לא\",\"כן\"]", "allowSkip":true, "mode":"buttons", "color":"#B2DFDB", "loc":"1465.96484375 17.74619140625"},
    {"text":"האם יש כאבי עצמות?", "key":8, "answers":"[\"לא\",\"כן\"]", "allowSkip":true, "mode":"buttons", "color":"#B2DFDB", "loc":"87.13671875 87.74619140625"},
    {"text":"האם יש מתן שתן מרובה?", "key":9, "answers":"[\"לא\",\"כן\"]", "allowSkip":true, "mode":"buttons", "color":"#B2DFDB", "loc":"295.595703125 87.74619140625"},
    {"text":"האם יש חולשה?", "key":10, "answers":"[\"לא\",\"כן\"]", "allowSkip":true, "mode":"buttons", "color":"#B2DFDB", "loc":"485 87.74619140625"},
    {"text":"מחלת כבד כרונית?", "key":11, "answers":"[\"לא\",\"כן\"]", "allowSkip":true, "mode":"buttons", "color":"#B2DFDB", "loc":"651.41650390625 87.74619140625"},
    {"text":"האם יש חום או זיהום?", "key":12, "answers":"[\"לא\",\"כן\"]", "allowSkip":true, "mode":"buttons", "color":"#B2DFDB", "loc":"839.2607421875 87.74619140625"},
    {"text":"עץ אקוטי על כרוני או כרוני", "key":13, "answers":"[]", "allowSkip":false, "mode":"buttons", "color":"#B2DFDB", "loc":"1051.65283203125 87.74619140625"},
    {"text":"האם היה שינוי במצב הכרה?", "key":14, "answers":"[\"לא\",\"כן\"]", "allowSkip":true, "mode":"buttons", "color":"#B2DFDB", "loc":"1285.89453125 87.74619140625"},
    {"text":"עץ שבצים", "key":15, "answers":"[]", "allowSkip":false, "mode":"buttons", "color":"#B2DFDB", "loc":"44.0615234375 157.74619140625"},
    {"text":"האם יש חולשה ביד, ברגל או בפנים", "key":16, "answers":"[\"לא\",\"כן\"]", "allowSkip":true, "mode":"buttons", "color":"#B2DFDB", "loc":"243.83251953125 157.74619140625"},
    {"text":"האם יש חולשה בחצי מהגוף?", "key":17, "answers":"[\"לא\",\"כן\"]", "allowSkip":true, "mode":"buttons", "color":"#B2DFDB", "loc":"513.51611328125 157.74619140625"},
    {"text":"האם יש שינוי או קושי בדיבור", "key":18, "answers":"[\"לא\",\"כן\"]", "allowSkip":true, "mode":"buttons", "color":"#B2DFDB", "loc":"759.5439453125 157.74619140625"},
    {"text":"עץ פרכוס", "key":19, "answers":"[]", "allowSkip":false, "mode":"buttons", "color":"#B2DFDB", "loc":"933.85302734375 157.74619140625"},
    {"text":"עץ עילפון", "key":20, "answers":"[]", "allowSkip":false, "mode":"buttons", "color":"#B2DFDB", "loc":"1038.00341796875 157.74619140625"},
    {"text":"עץ כאב ראש", "key":21, "answers":"[]", "allowSkip":false, "mode":"buttons", "color":"#B2DFDB", "loc":"1153.49609375 157.74619140625"},
    {"text":"עץ שינוי הכרה ללא כאב ראש", "key":22, "answers":"[]", "allowSkip":false, "mode":"buttons", "color":"#B2DFDB", "loc":"112.712890625 227.74619140625"},
    {"text":"האם יש פרכוס", "key":23, "answers":"[\"לא\",\"כן\"]", "allowSkip":true, "mode":"buttons", "color":"#B2DFDB", "loc":"305.95361328125 227.74619140625"},
    {"text":"האם התעלף או הכרה ירודה?", "key":24, "answers":"[\"לא\",\"כן\"]", "allowSkip":true, "mode":"buttons", "color":"#B2DFDB", "loc":"500.486328125 227.74619140625"},
    {"text":"האם יש כאב ראש?", "key":25, "answers":"[\"לא\",\"כן\"]", "allowSkip":true, "mode":"buttons", "color":"#B2DFDB", "loc":"711.85888671875 227.74619140625"},
    {"text":"תלונה עיקרית", "key":26, "answers":"[\"כאב ראש\",\"בלבול\",\"פרכוס\",\"עילפון\",\"חולשה או שיתוק בגוף\"]", "allowSkip":true, "mode":"buttons", "color":"#B2DFDB", "loc":"867.4208984375 227.74619140625"},
    {"text":"שאלות גנריות", "key":27, "answers":"[\"הבא\"]", "allowSkip":false, "mode":"buttons", "color":"#B2DFDB", "loc":"1004.0205078125 227.74619140625"},
    {"text":"מחלות ידועות ברקע", "key":28, "answers":"[\"הבא\"]", "allowSkip":true, "mode":"free-text", "color":"#B2DFDB", "loc":"1162.80810546875 227.74619140625"},
    {"text":"האם ידוע סרקודוזיס או בכצ'ט או מחלת כלי דם או כולסטרול גבוה או גידול בראש או גלוקומה או לופוס?", "key":29, "answers":"[\"לא\",\"כן\"]", "allowSkip":true, "mode":"buttons", "color":"#B2DFDB", "loc":"379.53564453125 297.74619140625"},
    {"text":"במעקב קבוע של אחד מהרופאים הבאים", "key":30, "answers":"[\"קרדיולוג\",\"אונקולוג\",\"אנדוקרינולוג\",\"המטולוג\",\"גסטרולוג\",\"ריאות\",\"נוירולוג\",\"אורטופד\",\"ראומוטולוג\",\"פסיכיאטר\",\"אחר\"]", "allowSkip":true, "mode":"buttons", "color":"#B2DFDB", "loc":"930.69775390625 297.74619140625"},
    {"text":"נוטל אחת מהתרופות הבאות", "key":31, "answers":"[\"תרופות למיגרנה\",\"משכחי כאבים\",\"אנטי אפילפטיות\",\"מדללי דם\",\"תרופות להרעות בקצב הלב\",\"סטטינים\"]", "allowSkip":true, "mode":"buttons", "color":"#B2DFDB", "loc":"1213.3759765625 297.74619140625"},
    {"text":"לוקחת גלולות נגד הריון?", "key":32, "answers":"[\"לא\",\"כן\"]", "allowSkip":true, "mode":"buttons", "color":"#B2DFDB", "loc":"1443.236328125 297.74619140625"},
    {"text":"נוטל תרופות קבועות?", "key":33, "answers":"[\"לא\",\"כן\"]", "allowSkip":true, "mode":"free-text", "color":"#B2DFDB", "loc":"1649.78369140625 297.74619140625"},
    {"text":"אלרגיות?", "key":34, "answers":"[\"לא\",\"כן\"]", "allowSkip":true, "mode":"free-text", "color":"#B2DFDB", "loc":"1800.5029296875 297.74619140625"},
    {"text":"האם מעשן על בסיס יומי?", "key":35, "answers":"[\"לא\",\"כן\"]", "allowSkip":true, "mode":"buttons", "color":"#B2DFDB", "loc":"1963.77294921875 297.74619140625"},
    {"text":"האם שותה אלכוהול יותר מפעם בשבוע?", "key":36, "answers":"[\"לא\",\"כן\"]", "allowSkip":true, "mode":"buttons", "color":"#B2DFDB", "loc":"152.92724609375 367.74619140625"},
    {"text":"במה עוסק?", "key":37, "answers":"[\"לא\",\"כן\"]", "allowSkip":true, "mode":"free-text", "color":"#B2DFDB", "loc":"376.2529296875 367.74619140625"},
    {"text":"הסוף!", "key":38, "answers":"[]", "allowSkip":false, "mode":"buttons", "color":"#B2DFDB", "loc":"476.49658203125 367.74619140625"},
    {"text":"מין", "key":"gender", "answers":"[\"נקבה\",\"זכר\"]", "allowSkip":true, "mode":"buttons", "color":"#B2DFDB", "loc":"238.923828125 -131.25380859375002"},
    {"text":"גיל", "key":"age", "answers":"[]", "allowSkip":false, "mode":"free-text", "color":"#B2DFDB", "loc":"614.3720703125 -127.25380859375002"},
    {"text":"חווה תאונת דרכים, נפילה או חבלה?", "key":"complaint", "answers":"[\"תאונת דרכים\",\"נפילה\",\"חבלה\",\"איבוד הכרה\",\"כאב ראש\"]", "allowSkip":true, "mode":"buttons", "color":"#b2dfdb", "loc":"805.7470703125 367.74619140625", "Comments":""},
    {"text":"מסוגל להזיז את גופו כרצונו?", "key":"movement", "answers":"[\"לא\",\"כן\"]", "allowSkip":true, "mode":"buttons", "color":"#B2DFDB", "loc":"158.462890625 -8.253808593750023"},
    {"text":"מסוגל לומר משפטים (גם אם מבולבל)?", "key":"sentences", "answers":"[\"לא\",\"כן\"]", "allowSkip":true, "mode":"buttons", "color":"#B2DFDB", "loc":"150.23779296875 437.74619140625"},
    {"text":"תלונה עיקרית", "key":"complaint2", "answers":"[\"כאב ראש\",\"בלבול\",\"פרכוס\",\"עילפון\",\"חולשה או שיתוק בגוף\",\"איבוד הכרה\",\"איבוד זכרון\",\"שינוי במצב הכרה או קוגניציה\"]", "allowSkip":true, "mode":"buttons", "color":"#B2DFDB", "loc":"501.669921875 576.74619140625"},
    {"text":"שינוי במצב הכרה / חולשה / כאב קרה באופן פתאומי", "key":"change", "answers":"[\"כן, חדש מהיום\",\"שינוי לאורך זמן\"]", "allowSkip":true, "mode":"buttons", "color":"#B2DFDB", "loc":"245.87548828125 554.74619140625"},
    {"text":"פוקח עיניים כשקוראים בשמו?", "key":"openEyes", "answers":"[\"לא\",\"כן\"]", "allowSkip":true, "mode":"buttons", "color":"#B2DFDB", "loc":"831.642578125 529.74619140625"}
],
"linkDataArray": [ 
    {"from":1, "to":"complaint", "color":"#5E35B1", "logic":"and", "subRules":"[{\"condition\":{\"questionId\":1,\"answer\":\"כן\"}}]"},
    {"from":1, "to":"movement", "color":"#5E35B1", "logic":"default"},
    {"from":2, "to":3, "color":"#5E35B1", "logic":"default"},
    {"from":3, "to":5, "color":"#5E35B1", "logic":"default"},
    {"from":4, "to":6, "color":"#5E35B1", "logic":"default"},
    {"from":5, "to":4, "color":"#5E35B1", "logic":"and", "subRules":"[{\"condition\":{\"questionId\":5,\"answer\":\"כן\"}}]"},
    {"from":5, "to":6, "color":"#5E35B1", "logic":"default"},
    {"from":6, "to":7, "color":"#5E35B1", "logic":"default"},
    {"from":7, "to":8, "color":"#5E35B1", "logic":"default"},
    {"from":8, "to":9, "color":"#5E35B1", "logic":"default"},
    {"from":9, "to":10, "color":"#5E35B1", "logic":"default"},
    {"from":10, "to":11, "color":"#5E35B1", "logic":"default"},
    {"from":11, "to":12, "color":"#5E35B1", "logic":"default"},
    {"from":12, "to":"change", "color":"#5E35B1", "logic":"default"},
    {"from":13, "color":"#5E35B1", "logic":"default"},
    {"from":14, "to":"complaint2", "color":"#5E35B1", "logic":"and", "subRules":"[{\"condition\":{\"questionId\":14,\"answer\":\"לא\"}}]"},
    {"from":14, "to":12, "color":"#5E35B1", "logic":"and", "subRules":"[{\"condition\":{\"questionId\":14,\"answer\":\"כן\"}}]"},
    {"from":14, "to":"complaint2", "color":"#5E35B1", "logic":"default"},
    {"from":15, "color":"#5E35B1", "logic":"default"},
    {"from":16, "to":17, "color":"#5E35B1", "logic":"default"},
    {"from":17, "to":18, "color":"#5E35B1", "logic":"default"},
    {"from":18, "to":15, "color":"#5E35B1", "logic":"or", "subRules":"[{\"condition\":{\"questionId\":16,\"answer\":\"כן\"}},{\"condition\":{\"questionId\":17,\"answer\":\"כן\"}},{\"condition\":{\"questionId\":18,\"answer\":\"כן\"}}]"},
    {"from":18, "to":23, "color":"#5E35B1", "logic":"default"},
    {"from":19, "color":"#5E35B1", "logic":"default"},
    {"from":20, "color":"#5E35B1", "logic":"default"},
    {"from":21, "color":"#5E35B1", "logic":"default"},
    {"from":22, "color":"#5E35B1", "logic":"default"},
    {"from":23, "to":19, "color":"#5E35B1", "logic":"and", "subRules":"[{\"condition\":{\"questionId\":23,\"answer\":\"כן\"}}]"},
    {"from":23, "to":24, "color":"#5E35B1", "logic":"default"},
    {"from":24, "to":20, "color":"#5E35B1", "logic":"and", "subRules":"[{\"condition\":{\"questionId\":24,\"answer\":\"כן\"}}]"},
    {"from":24, "to":25, "color":"#5E35B1", "logic":"default"},
    {"from":25, "to":22, "color":"#5E35B1", "logic":"and", "subRules":"[{\"condition\":{\"questionId\":25,\"answer\":\"לא\"}}]"},
    {"from":25, "to":21, "color":"#5E35B1", "logic":"and", "subRules":"[{\"condition\":{\"questionId\":25,\"answer\":\"כן\"}}]"},
    {"from":25, "to":21, "color":"#5E35B1", "logic":"default"},
    {"from":26, "to":27, "color":"#5E35B1", "logic":"default"},
    {"from":27, "to":28, "color":"#5E35B1", "logic":"default"},
    {"from":28, "to":29, "color":"#5E35B1", "logic":"default"},
    {"from":29, "to":30, "color":"#5E35B1", "logic":"default"},
    {"from":30, "to":31, "color":"#5E35B1", "logic":"default"},
    {"from":31, "to":32, "color":"#5E35B1"},
    {"from":31, "to":33, "color":"#5E35B1", "logic":"default"},
    {"from":32, "to":33, "color":"#5E35B1", "logic":"default"},
    {"from":33, "to":34, "color":"#5E35B1", "logic":"default"},
    {"from":34, "to":35, "color":"#5E35B1", "logic":"default"},
    {"from":35, "to":36, "color":"#5E35B1", "logic":"default"},
    {"from":36, "to":37, "color":"#5E35B1", "logic":"default"},
    {"from":37, "to":38, "color":"#5E35B1", "logic":"default"},
    {"from":38, "color":"#5E35B1", "logic":"default"},
    {"from":"gender", "to":"age", "color":"#5E35B1", "logic":"default"},
    {"from":"age", "to":1, "color":"#5E35B1", "logic":"default"},
    {"from":"complaint", "to":"movement", "color":"#5E35B1", "logic":"default"},
    {"from":"movement", "to":"sentences", "color":"#5E35B1", "logic":"default"},
    {"from":"sentences", "to":"openEyes", "color":"#5E35B1", "logic":"default"},
    {"from":"complaint2", "to":27, "color":"#5E35B1", "logic":"default"},
    {"from":"change", "to":16, "color":"#5E35B1", "logic":"and", "subRules":"[{\"condition\":{\"questionId\":\"change\",\"answer\":\"כן, חדש מהיום\"}}]"},
    {"from":"change", "to":13, "color":"#5E35B1", "logic":"and", "subRules":"[{\"condition\":{\"questionId\":\"change\",\"answer\":\"שינוי לאורך זמן\"}}]"},
    {"from":"change", "to":26, "color":"#5E35B1", "logic":"default"},
    {"from":"openEyes", "to":14, "color":"#5E35B1", "logic":"and", "subRules":"[{\"condition\":{\"questionId\":\"movement\",\"answer\":\"כן\"}},{\"condition\":{\"questionId\":\"sentences\",\"answer\":\"כן\"}},{\"condition\":{\"questionId\":\"openEyes\",\"answer\":\"כן\"}}]"},
    {"from":"openEyes", "to":2, "color":"#5E35B1", "logic":"default"}
]};


module.exports = {createTree};
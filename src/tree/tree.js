const treeBuilder = require('./treeBuilder');
const graphCreator = require('../graph/graphCreator');
// const treeBuilder2 = require('../graph/treeBuilder2');
let questionMap;
let ruleOutMap;
let inputs;
let sideChains;

function init() {
    const inputElement = document.getElementById("load-file");
    inputElement.addEventListener("change", loadFile, false);
}

function createGraph(json) {
    // questionMap = treeBuilder.createQuestions();
    questionMap = treeBuilder.createQuestions(json);
    ruleOutMap = treeBuilder.getRuleOutMap();
    inputs = treeBuilder.getInputMap();
    //questionMap = treeBuilder2.createTree();
    let graph = graphCreator.createGraph(Object.values(questionMap));
    console.log(graph);

    // Map side-chains
    createSideChainsMapping();
}

function createSideChainsMapping() {
    sideChains = {};
    for (let key in questionMap) {
        let question = questionMap[key];
        if (!question.rules)
            continue;

        question.rules.forEach(rule => {
            let baseQuestionOfFromQuestion = getBaseQuestionId(question.id);
            if (rule.type === 'side-chain') {
                addToSideChains(question.id, rule.nextId);
            }
            else if (rule.type === 'next-question' && baseQuestionOfFromQuestion) {
                addToSideChains(baseQuestionOfFromQuestion, rule.nextId);
            }
        });
    }
}

function addToSideChains(baseQuestionId, questionId) {
    if (!sideChains[baseQuestionId]) {
        sideChains[baseQuestionId] = [];
    }
    sideChains[baseQuestionId].push(questionId);
}

function getBaseQuestionId(questionId) {
    for (let key in sideChains) {
        let sideChain = sideChains[key];
        if (sideChain.includes(questionId)) {
            return key;
        }
    }
}

function loadFile() {
    let file = this.files[0];
    let reader = new FileReader();
    reader.readAsText(file, "UTF-8");
    reader.onload = loaded;
}

function loaded(evt) {
    var fileString = evt.target.result;
    let json = JSON.parse(fileString);
    createGraph(json);
}

function getAllQuestions() {
    return questionMap;
}

function getQuestion(id) {
    return questionMap[id];
}

function getRuleOut(id) {
    return ruleOutMap[id];
}

function getAllInputs() {
    return inputs;
}

function getInput(id) {
    return inputs[id];
}

function getFirst() {
    let first = Object.values(questionMap).find(question => question.isFirst);
    return first || questionMap["-1"];
}

function getLast() {
    // TODO: We can scan the tree in order to determine where it ends (where we have a question with no rules from it)
    return ["-149", "-152"];
}

module.exports = {init, getQuestion, getFirst, getRuleOut, getAllQuestions, getAllInputs, getInput, getLast, getBaseQuestionId};



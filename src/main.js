let tree = require('./tree/tree');

function init() {
    tree.init();
}

init();

require('./angular/angularModule');
require('./angular/angularFileLoader');
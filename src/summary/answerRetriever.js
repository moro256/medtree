const summary = require('./summary');
const tree = require('../tree/tree');

function getAnswer(id) {
    if (!summary.get(id)) {
        return null; // TODO: ?
    }
    
    if (summary.get(id).answer) {
        return summary.get(id).answer.str;
    } else if (summary.get(id).selectedAnswers) {
        return summary.get(id).selectedAnswers;
    }

    // TODO: If id is of a ruleout or alert - return the score.
    let ruleOut = summary.getRuleOuts(id);
    if (ruleOut && ruleOut.score && (ruleOut.score.value || ruleOut.score.value==0)) {
        return ruleOut.scorer.value;
    }

    return null;  // TODO: ?
}

function getAllQuestionsAndAnswers() {
    return tree.getAllQuestions();
}

function getAllRuleOuts() {
    return summary.getRuleOuts();
}

module.exports = {getAnswer, getAllQuestionsAndAnswers, getAllRuleOuts};
const answerRetriever = require('../answerRetriever');

function create() {
    let story= "";
    let g17 = answerRetriever.getAnswer("g17");
    if (g17) {
        story += "מחלות ידועות במשפחה: " + g17;
    }
    return story;
}

module.exports = {create};
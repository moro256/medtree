const stroke = require('./stroke');
function create() {
    let story = ""
    if (stroke.isStroke()) {
        story+= "השלמת אנמנזה מלאה, בדיקה נוירולוגית, השלמת NIH strokes scale, non contrast CT";
    }

    return story;

}

module.exports = {create};
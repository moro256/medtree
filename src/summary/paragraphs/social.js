const answerRetriever = require('../answerRetriever');

function create() {
    let story = "";
    let g9 = answerRetriever.getAnswer("g9");
    let g9_1 = answerRetriever.getAnswer("g9_1");
    let g9_2 = answerRetriever.getAnswer("g9_2");

    if (g9 == "כן") {
        story += "שתיית אלכוהול מסוג: " + g9_1 + " ";
        story += " בתדירות שבועית" + g9_2 + ", ";
    }
    let g10 = answerRetriever.getAnswer("g10");
    let freqPerDay = answerRetriever.getAnswer("g10_1");
    let timesAday = answerRetriever.getAnswer("g10_2");
    let numberOfCigaretesInPack = 20;
    let packYears = freqPerDay / numberOfCigaretesInPack * timesAday;

    if (g10 == "כן") {
        story += "מעשן " + packYears + " שנות חפיסה, ";
    }


    let g11 = answerRetriever.getAnswer("g11");
    if (g11) {
        story += "מקצוע " + g11 + ", ";
    }

    let g14 = answerRetriever.getAnswer("g14");
    let g14_1 = answerRetriever.getAnswer("g14_1");
    if (g14 == "כן") {
        story += "בביתו גרים: " + g14_1 + ", ";
    } else if (g14 == "לא") {
        story += "מתגורר לבד בביתו ,";
    }

    let g15 = answerRetriever.getAnswer("g15");
    if (g15 == "זקוק לעזרה ביומיום") {
        story += "זקוק לעזרה ביום יום, ";
    }
    
    let g16 = answerRetriever.getAnswer("g16");
    let g16_1 = answerRetriever.getAnswer("g16_1");
    if (g16 == "כן") {
        story += "מקבל " + g16_1 + " שעות מביטוח לאומי, ";
    }

    return story;
}

module.exports = {create}
const answerRetriever = require('../answerRetriever');

function create() {
    let story = "";
    let g8 = answerRetriever.getAnswer("g8");
    let g8_1 = answerRetriever.getAnswer("g8_1");

    if (g8 == "כן") {
        story += "אושפז בעבר: " + g8_1 + " ";
    }

    return story;
}

module.exports = {create};
const answerRetriever = require('../answerRetriever');

function create() {
    let story = "";
    let q2016 = answerRetriever.getAnswer("q2016");
    if (q2016 == "כן") {
        story += "נוטל מדללי דם או נוגדי טסיות, ";
    }

    let g1_15 = answerRetriever.getAnswer("g1_15");
    if (g1_15 == "כן") {
        story += "נוטל/ת תוספי אשלגן/מגנזיום, ";
    }

    let g1_18 = answerRetriever.getAnswer("g1_18");
    if (g1_18 == "כן") {
        story += "נוטל/ת תרופות מדכאות חיסונית, ";
    }
    
    let gDDX2 = answerRetriever.getAnswer("gDDX2");
    if (gDDX2 && gDDX2 != "לא") {
        story += "נוטל/ת " + gDDX2 + " ";
    }

    let g4 = answerRetriever.getAnswer("g4");
    if (g4 && g4 == "כן") {
        story += "לוקחת גלולות למניעת הריון, ";
    }

    let g5_1 = answerRetriever.getAnswer("g5_1");
    if (g5_1) {
        story += "נוטל/ת " + g5_1 + ", ";
    }

    let g5followup_1 = answerRetriever.getAnswer("g5followup_1");
    if (g5followup_1) {
        story += "נוטל/ת " + g5followup_1 + ", ";
    }

    return story;
}

module.exports = {create};
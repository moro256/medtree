const RuleOutProbability = {
    VERY_WEAK: 0,
    WEAK: 1,
    NEUTRAL: 2,
    STRONG : 3,
    VERY_STRONG : 4,
    MUST: 5
};

const WEAK_TEXT = "סבירות נמוכה";
const STRONG_TEXT = "סבירות בינונית";
const VERY_STRONG_TEXT = "סבירות גבוהה";
const MUST_TEXT = "סבירות גבוהה מאוד";
const UNKNOWN_STRENGTH_TEXT = "סבירות לא ידועה";

function get(ordinal) {
    switch (ordinal) {
        case 0:
            return RuleOutProbability.VERY_WEAK;
        case 1:
            return RuleOutProbability.WEAK;
        case 2:
            return RuleOutProbability.NEUTRAL;
        case 3:
            return RuleOutProbability.STRONG;
        case 4:
            return RuleOutProbability.VERY_STRONG;
        case 5:
            return RuleOutProbability.MUST;
    }
}

module.exports={RuleOutProbability, get, WEAK_TEXT, STRONG_TEXT, VERY_STRONG_TEXT, MUST_TEXT, UNKNOWN_STRENGTH_TEXT};
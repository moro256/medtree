const consts = require('../consts');

let answers = {};
let ruleOuts = []; // This is the actions as well
let visitedNodes = [];

function restoreAnswers(question, restoredAnswers) {
    if (Array.isArray(restoredAnswers)) {
        answers[question.id] = {question: question, selectedAnswers: restoredAnswers};
    } else {
        answers[question.id] = {question: question, answer: restoredAnswers};
    }

    // Set a.selected and a.isAnswered on the answers of the question as well
    if (!question.answers)
        return;

    if (Array.isArray(restoredAnswers)) {
        question.answers.forEach(old => {
            restoredAnswers.forEach(restored => {
                if (old.str == restored.str) {
                    old.selected = restored.selected;
                    old.isAnswered = restored.isAnswered;
                }
            }) 
        });
    } else {
        question.answers.forEach(old => {
            if (restoredAnswers.str == old.str) {
                old.selected = restoredAnswers.selected;
                old.isAnswered = restoredAnswers.isAnswered;
            }
        });
    }
}

function clone(obj) {
    if (null == obj || "object" != typeof obj) return obj;
    var copy = obj.constructor();
    for (var attr in obj) {
        if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
    }
    return copy;
}

function clearAnswers(question) {
    let answer = answers[question.id];
    if (!answer)
        return;

    let allAnswers = answer.question.answers;
    if (allAnswers) {
        let newAnswers = [];
        allAnswers.forEach(a => {
            let newAnswerObj = clone(a);
            newAnswerObj.selected = false;
            newAnswerObj.isAnswered = false;
            newAnswers.push(newAnswerObj);
        });
        answer.question.answers = newAnswers;
    }
    else if (answer.answer) {
        // answer.answer.selected = false;
        answer.answer = null;
    }

    answers[question.id] = null;
    delete answers[question.id];
}

function setEmptyAnswer(question) {
    answers[question.id] = {question: question, selectedAnswers: []};
}

function append(question, answer) {
    if (question.mode==consts.questionMode.MULTI_CHOICE) {
        let selectedAnswers;
        if (answers[question.id]) {
            selectedAnswers = answers[question.id].selectedAnswers;
            if (answer.selected || question.multiChoiceYesNo) { // Add answer
                selectedAnswers.push(answer);
                selectedAnswers = [...new Set(selectedAnswers)];
            } else { // Remove answer
                const index = selectedAnswers.indexOf(answer);
                if (index > -1) {
                    selectedAnswers.splice(index, 1);
                }
            }
        } else {
            selectedAnswers = [answer];
            answers[question.id] = {question, selectedAnswers};
        }
        answers[question.id] = {question, selectedAnswers};
        
    }
    else {
        answers[question.id] = {question, answer};
    }
    //console.log(print());
}

function get(id) {
    if (id) {
        return answers[id];
    }
}

function getMap() {
    return answers;
}

function getRuleOuts() {
    return ruleOuts;
}

function restoreSnapshot(snapshot) {
    setRuleOuts(snapshot.actions);
    setVisitedNodes(snapshot.visitedNodes);
}

// TODO: This whole "new set" thing only worked for IDs.. now it's not useful at all
// TODO: If you change this shit to a map (for R/O strength) - make sure you change the questionController snapshots as well!
function setRuleOuts(newRuleOuts) {
    ruleOuts = [...new Set(newRuleOuts)];
}

function addRuleOuts(ruleOutsToAdd) {
    ruleOutsToAdd.forEach(ro => ruleOuts.push(ro));
    ruleOuts = [...new Set(ruleOuts)];
}

function addRuleOut(ruleOut) {
    ruleOuts.push(ruleOut);
    ruleOuts = [...new Set(ruleOuts)];
}

function removeRuleOut(ruleOut) {
    let index = ruleOuts.indexOf(ruleOut);
    if (index != -1) {
        ruleOuts.splice(index, 1);
    }
}

function setVisitedNodes(newVisitedNodes) {
    visitedNodes = [...new Set(newVisitedNodes)];
}

function setVisitedNode(nodeId, isVisited) {
    if (isVisited) {
        visitedNodes.push(nodeId);
    }
    else { // Remove visited node
        const index = visitedNodes.indexOf(nodeId);
        if (index > -1) {
            visitedNodes.splice(index, 1);
        }
    }
}

function getVisitedNodes() {
    return visitedNodes;
}

function isNodeVisited(nodeId) {
    return visitedNodes.includes(nodeId);
}

function print() {
    let str = "";
    Object.keys(answers).forEach(questionId => {
        let item = answers[questionId];
        str += `\n${item.question.questionStr}:${item.answer.str}`;
    });
    return str;
}

module.exports = {append, clearAnswers, setEmptyAnswer, get, getMap, print, addRuleOuts, addRuleOut, removeRuleOut, setVisitedNode, isNodeVisited, getRuleOuts, getVisitedNodes, restoreSnapshot, restoreAnswers}
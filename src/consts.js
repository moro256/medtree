consts = {
    questionMode: {
        BUTTONS: "buttons",
        FREE_TEXT: "free-text",
        MULTI_CHOICE: "multi-answer"
    }
}

module.exports = consts;